import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = { //要设置的全局访问的state对象,赋予初始属性值
    ComponentType: '首页',
    TestPicWorkData:[],
    wellParamsData:[],
    wellBudgetData:[],
    FootComShowState:false,
    CompanyName:"",
    TableName:"",
    ShowRightDialogData:""
  
    
};
const getters = {   //实时监听state值的变化(最新状态)
 
};
// const mutations = {
//     //自定义改变state初始值的方法，这里面的参数除了state之外还可以再传额外的参数(变量或对象);
//     clearCatch(state) { 
//         state.cache = "";
//         state.changeThemeCount= 0;
//     },
//     setThemeColor(state,color,opacity){ 
//        state.themeColor.val = color;
//        state.themeColor.opacity = opacity;
//        state.changeThemeCount++;
//     }
// };
// const store = new Vuex.Store({
//         state, // 挂载存取数据功能
//        getters, //挂载数据计算功能
//        mutations // 挂载函数功能
// });
// export default store;


const store = new Vuex.Store({
    // 定义状态
    //  state: {
    //  ComponentType: '首页',
    //  TestPicWorkData:[],
    //  mutations: {
    //     newComponentType(state,msg){
    //         state.ComponentType = msg;
    //     },
    //     newTestPicWorkData(state,msg){
    //       state.TestPicWorkData = msg;
    //   },
    //  },
    //  modules: {
    //     ContainComMod:containComMod
    //  }
    state,
    getters
})

export default store;