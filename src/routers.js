import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      component: Login,
      name: '',
      hidden: true
    }, {
      path: '/Home',
      component: Home,
      name: '导航一',
      //iconCls: 'el-icon-message',//图标样式class
    },
  ]
})
