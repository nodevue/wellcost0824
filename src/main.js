
import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App'
import router from './routers'
import Vuex from 'vuex'
import store from './vuex/store.js'
import XLSX from 'xlsx';
import VueContextMenu from '@xunlei/vue-context-menu'
import VueResource from 'vue-resource'
import axios from 'axios'

import getDataFun from './vuex/getDataFun.js'

/*使用VueResource插件*/
Vue.use(Vuex)
Vue.use(ElementUI)
Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(XLSX)
Vue.use(VueContextMenu)
Vue.prototype.getDataFun = getDataFun;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template:'<App/>',
})

